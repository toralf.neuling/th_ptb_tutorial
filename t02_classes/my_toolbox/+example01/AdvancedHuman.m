classdef AdvancedHuman
  properties (SetAccess=private)
    name
  end
  
  methods
    function obj = AdvancedHuman(name)
      fprintf('Creating a nice Human called %s for you.\n', name);
      obj.name = name;
    end %function
    
    function hello(obj)
      disp('Hi!');
    end %function
    
    function say_name(obj)
      disp(['My name is ' obj.name '!']);
    end %function
  end
  
end


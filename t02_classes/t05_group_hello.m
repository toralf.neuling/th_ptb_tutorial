%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_toolbox');

%% get some pirates and humans
my_pirate = example01.Pirate('Adam');

my_first_human = example01.AdvancedHuman('Mary');
my_second_human = example01.AdvancedHuman('Eve');

%% send them all to the group_hello function
all_humans = {my_pirate, my_first_human, my_second_human};

example01.group_hello(all_humans);
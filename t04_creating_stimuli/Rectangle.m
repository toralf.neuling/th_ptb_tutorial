classdef Rectangle < th_ptb.stimuli.visual.Base
  %RECTANGLE Summary of this class goes here
  %   Detailed explanation goes here
  
  properties (Access=protected)
    width;
    height;
    color;
  end %properties
  
  methods
    function obj = Rectangle(width, height, color)
      obj@th_ptb.stimuli.visual.Base();
      
      ptb = th_ptb.PTB.get_instance;
      
      obj.width = width;
      obj.height = height;
      obj.color = color;
      obj.destination_rect = CenterRect([0 0 obj.width obj.height], ptb.win_rect);
    end %function
    
    function on_draw(obj, ptb)
      ptb.screen('FillRect', obj.color, obj.destination_rect);
    end %function
  end
  
end

